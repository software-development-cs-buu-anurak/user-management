/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.anurak.usermanagement;

/**
 *
 * @author anu01
 */
public class TestUserService {
    public static void main(String[] args) {
        
        //Create
        UserService.addUser("user2","password");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user3","password"));
        System.out.println(UserService.getUsers());
        
        User user = UserService.getUser(0);
        System.out.println(user);
        
        //Update
        user.setPassword("1234");
        System.out.println(user);
        
        //Del
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
        //Login
        System.out.println(UserService.login("user2", "password"));
        System.out.println("");
    }
}
